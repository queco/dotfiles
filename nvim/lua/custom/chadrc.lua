---@type ChadrcConfig
local M = {}
M.ui = {
  theme = "onenord",
  nvdash = {
    load_on_startup = true,
  },
}

M.plugins = "custom.plugins"

M.mappings = require "custom.mappings"

vim.opt.iskeyword:append "-"
vim.opt.relativenumber = true

return M
