local M = {}

-- Custom mappings
M.abc = {
  n = {
    ["<leader>j"] = { "<cmd> :m .+1<CR>==", "Move line down" },
    ["<leader>k"] = { "<cmd> :m .-2<CR>==", "Move line up" },

    ["<leader><Down>"] = { "<cmd> :m .+1<CR>==", "Move line down" },
    ["<leader><Up>"] = { "<cmd> :m .-2<CR>==", "Move line up" },
  },
}

return M
